// USES VUE.JS
// USES JQUERY

/*global $*/
/*global Vue*/

var __control_table_default_plugin_id = 'plugin-control-table';
var __control_table_csrf_token = null;

// Colapse control
var __control_table_colapsed; // Colapsed items array

// General control
var __control_table_available_lists;

// Week Tabs Control
var __control_table_active_tab = -1;

// Month Changing control
var __control_table_month_offset = 0;
var __control_table_month_fetch_url = null;
var __control_table_month_control_view = null;
var __control_table_actual_month = new Date();


$(document).ready(function() {
	$("#"+__control_table_default_plugin_id).show();
	__control_table_init();
});

// ------------------------------------------
// Laravel Functions
// ------------------------------------------

function control_table_set_token(token)
{
	__control_table_csrf_token = token;
}

function control_table_set_fetch_view(view)
{
	__control_table_month_control_view = view;
}


// ------------------------------------------
// Initialization Function
// ------------------------------------------

function __control_table_init()
{
	// ------------------------------------------
	// INITIALIZATION OF VARIABLES
	// ------------------------------------------
	// Colapse control
	__control_table_colapsed = new Array(); // Colapsed items array

	// General control
	__control_table_available_lists = new Array();

	// ------------------------------------------
	// INITIALIZATION OF CONTROLLERS
	// ------------------------------------------

	// Filter controller
	if($("select[name=control-table-filter]").size() > 0)
		$("select[name=control-table-filter]").on("keydown change", function() {
			__control_table_change_filter($(this).val());
		});
	
	// If months are enabled,
	if($("[id*=control-table--month]").size() > 0)
	{
		// Month changing controllers
		$("[id=control-table--month-back]").click(function() {
			__control_table_month_offset--;
			__control_table_load_month(__control_table_month_offset);
		});


		$("[id=control-table--month-forward]").click(function() {
			__control_table_month_offset++;
			__control_table_load_month(__control_table_month_offset);
		});
	}





	// ------------------------------------------
	// INITIALIZATION OF DAY-LISTS
	// ------------------------------------------

	// If week tabs are enabled,
	if($("ul#control-table-week-tabs").size() > 0)
	{
		// Placing each list in correct week tab
		var month = new Date();
		__control_table_actual_month.setMonth(__control_table_actual_month.getMonth() + __control_table_month_offset);
		var firstDay = new Date(__control_table_actual_month);
		firstDay.setMonth(firstDay.getMonth() + __control_table_month_offset);
		firstDay.setDate(1);
		__control_table_active_tab = Math.ceil((firstDay.getDay() + month.getDate())/7);

		$("div.control-table-days").each(function(key, table) {
			// Creates date object using the date set by the programmer
			var date = new Date($(table).data('date'));

			var tab = Math.ceil((firstDay.getDay() + date.getDate())/7);

			$(table).appendTo("div#control-table-week-tab-" + tab);

			if(tab != __control_table_active_tab)
			{
				$(table).parent().hide();
			}
		});

		// Setting triggers for each tab
		$("ul#control-table-week-tabs li").each(function(key, elem) {
			$(elem).click(function() {
				var tab = $(elem).data('tab');

				if(tab != __control_table_active_tab)
				{
					$("ul#control-table-week-tabs li.active").removeClass('active');
					$("div#control-table-week-tab-" + __control_table_active_tab).hide('fade');
					$(elem).addClass('active');
					$("div#control-table-week-tab-" + tab).show('fade');
					__control_table_active_tab = tab;
				}
			});
		});
	}


	// Getting lists for each tab
	var index = 0;
	$("div.control-table-days").each(function(key, table) {
		// Sets this table's index
		$(table).data("index", index++);
		var $lists = $(table).find("[id*='control-table--weekday']");
		// Removal of empty days
		var num_lists = $($lists).size();
		for(var i = num_lists-1; i >= 0; i--)
		{
			if($($lists[i]).children().size() == 0)
			{
				var id = $($lists[i]).data('id');
				$($lists[i]).closest('.control-table').find("[id="+ id +"]").remove();
				$($lists[i]).remove();
				$lists.splice(i, 1);
				num_lists--;
			}
		}

		__control_table_available_lists.push($lists);
	});
}

// ------------------------------------------
// MONTH CHANGING CONTROL
// ------------------------------------------

function control_table_set_fetch_url(url)
{
	__control_table_month_fetch_url = url;
}

function __control_table_load_month(offset)
{
	if(__control_table_month_fetch_url)
	{
		$.ajax({
			url: __control_table_month_fetch_url,

			type: "POST",

			data: {
				_token: __control_table_csrf_token,
				offset: offset,
				view: __control_table_month_control_view
			},

			success: function(response) {
				// Sets data
				$("div.control-table").children().remove();
				$("div.control-table").html(response.data);

				// Resets Vue
				__control_table_vue_reset();

				// Reinitializes plugin
				__control_table_init();
				
				// Resets popovers if exists
				if((typeof popover_module_init) == 'function')
					popover_module_init();
			},

			error: function() {
				console.log("ERROR: Could not load data for offset ", offset);
			}
		});
	}
}



// ------------------------------------------
// FILTERING
// ------------------------------------------

function __control_table_change_filter(filter)
{
	var $lists = $("[id*=control-table--weekday]");
	
	if(filter)
	{
		// Resets filter
		$lists.each(function(key, item) {
			var toHide = $(item).find("li[data-state!=" + filter + "]");
			var i = 0;

			$(toHide[i++]).children().hide('fast', function() {
				$(toHide[i++]).children().hide('fast', arguments.callee);
			});
		});
		
		$lists.each(function(key, item) {
			var toShow = $(item).find("li[data-state=" + filter + "]");
			var i = 0;

			$(toShow[i++]).children().show('fast', function() {
				$(toShow[i++]).children().show('fast', arguments.callee);
			});
		});
	}
	else
	{
		$lists.each(function(key, item) {
			var toShow = $(item).find("li");
			var i = 0;

			$(toShow[i++]).children().show('fast', function() {
				$(toShow[i++]).children().show('fast', arguments.callee);
			});
		});
	}
}





// ------------------------------------------
// AUTO COLAPSE
// ------------------------------------------

function control_table_uncolapse_items(filter)
{
	if(typeof __control_table_colapsed[filter] != 'undefined' && __control_table_colapsed[filter])
	{
		// Shows colapsed <li> tags
		for(var i = 0; i < __control_table_colapsed[filter].length; i++)
			$(__control_table_colapsed[filter][i]).slideDown();
		__control_table_colapsed[filter] = null;
		
		var $parent = $("[data-toggle=control-table-uncolapse][data-filter="+ filter +"]").closest("li.control-table-colapsed");
		var line = $parent.index();
		
		for(var i = 0; i < __control_table_available_lists.length; i++)
		{
			$($(__control_table_available_lists[i])).each(function(key, elem) {
				$(elem).find("li.control-table-colapsed").remove();
			});
		}
		
		$parent.remove();
	}
}

function control_table_toggle_colapse(element)
{
	var colapsed = $(element).data('colapsed');
	var filter = $(element).data('filter');
	var i = $(element).data('index');

	if(colapsed == true)
	{
		// Show items again
		for(var k = 0; k < __control_table_colapsed[filter][i].length; k++)
		{
			$(__control_table_colapsed[filter][i][k]).slideDown();
		}

		$(element).addClass('rotate-right');
		$(element).data('colapsed', false);
	}
	else
	{
		// Hide items again
		for(var k = 0; k < __control_table_colapsed[filter][i].length; k++)
		{
			$(__control_table_colapsed[filter][i][k]).slideUp();
		}

		$(element).removeClass('rotate-right');
		$(element).data('colapsed', true);
	}
}

function __control_table_make_colapse(lines, filter, table)
{
	// Only colapses if there are at least two consecutive 'empty' lines
	if(lines.length > 1)
	{
		var $lists = $(table).find(".control-table-days--body > ul");

		if(typeof __control_table_colapsed[filter] == 'undefined' || !__control_table_colapsed[filter])
			__control_table_colapsed[filter] = new Array();
		__control_table_colapsed[filter].push(new Array());
		var index = __control_table_colapsed[filter].length - 1;

		for(var i = lines.length-1; i >= 0; i--)
		{
			$lists.each(function(key, item) {
				$($(item).children()[lines[i]]).hide();
				__control_table_colapsed[filter][index].push($(item).children()[lines[i]]);
			});
		}
		
		// Getting first and last timestamp
		var timestamp = $($($lists[0]).children()[lines[0]]).find(".control-table-timestamp").html().substr(0, 5);
		timestamp += " - " + $($($lists[0]).children()[lines[lines.length - 1]]).find(".control-table-timestamp").html().substr(8, 5);
		
		// Adding 
		var timestampToPrepend = "\
			<li class=\"control-table-colapsed\">\
				<p data-id=\"" + timestamp +"\" class=\"control-table-timestamp\">"+ timestamp +"</p>\
					<i class=\"pull-right md md-keyboard-arrow-right control-table-toggle-colapse\" \
					data-toggle=\"control-table-uncolapse\" \
					data-filter=\""+ filter +"\" \
					data-colapsed=\"true\" \
					data-index=\"" + index + "\"></i> \
			</li>";
			
		var dataToPrepend = "\
			<li class=\"control-table-colapsed\">\
			</li>";
		
		$($($lists[0]).children()[lines[0]]).before(timestampToPrepend);
		
		for(var i = 1; i < $lists.size(); i++)
			$($($lists[i]).children()[lines[0]]).before(dataToPrepend);
	}
}

function control_table_colapse_items(filter)
{
	$(document).ready(function() {
		var may_colapse;
		
		$("div.control-table-days").each(function(key, table) {
			var $lists = __control_table_available_lists[$(table).data("index")];
			
			var num_lists = $($lists).size();
			var list_length = $($lists[0]).children().size();
			
			var colapsible_items = new Array();
			var may_colapse;
			var i;

			for(i = 0; i < list_length; i++)
			{
				may_colapse = true;
				for(var j = 0; j < num_lists; j++)
				{
					var $item = $($lists[j]).children("li")[i];
					var state = $($item).data('state');

					if(state && (state.indexOf(filter) == -1) && (state.indexOf('any') == -1))
						may_colapse = false;
					
				} // for(j)
					
				if(!may_colapse)
				{
					__control_table_make_colapse(colapsible_items, filter, table);
					colapsible_items = new Array();
				}
				else
				{
					colapsible_items.push(i);
				}
			} // for(i)
			 
			// Last iteration
			__control_table_make_colapse(colapsible_items, filter, table);
			colapsible_items = null;
			
			__control_table_reset_colapse_controller();
		});
	});
}

function __control_table_reset_colapse_controller()
{
	$("[data-toggle=control-table-uncolapse]").off('click');
	$("[data-toggle=control-table-uncolapse]").click(function() {
		control_table_toggle_colapse(this);
	});
}

// ------------------------------------------
// TEMPLATES
// ------------------------------------------

Vue.component('modal-body', {
	template: "#" + __control_table_default_plugin_id + "--modal-template"
});

Vue.component('modal-call-button', {
	template: "#" + __control_table_default_plugin_id + "--modal-call-button-template"
});

Vue.component('button-with-popover', {
	template: "#" + __control_table_default_plugin_id + "--data-popover-template"
});

Vue.component('control-table', {
	template: "#" + __control_table_default_plugin_id + "--table-template"
});

Vue.component('control-table-months', {
	template: "#" + __control_table_default_plugin_id + "--month-component"
});

Vue.component('control-table-weeks', {
	template: "#" + __control_table_default_plugin_id + "--week-component"
});

Vue.component('control-table-days', {
	template: "#" + __control_table_default_plugin_id + "--day-component"
});

var mainBody = new Vue({
	el: "#" + __control_table_default_plugin_id
});



// ------------------------------------------
// VUE
// ------------------------------------------

function __control_table_vue_reset()
{
	$("#" + __control_table_default_plugin_id).hide('fade');
	mainBody = new Vue({
		el: "#" + __control_table_default_plugin_id
	});
	$("#" + __control_table_default_plugin_id).show('fade');
}