/**
* Theme: Ubold Admin Template
* Author: Coderthemes
* Component: Full-Calendar
*
*/

!function($) {
    "use strict";

    var CalendarApp = function() {
        this.$body = $("body")
        this.$modal = $('#event-modal'),
        this.$event = ('#external-events div.external-event'),
        this.$calendar = $('#calendar'),
        this.$saveCategoryBtn = $('.save-category'),
        this.$categoryForm = $('#add-category form'),
        this.$extEvents = $('#external-events'),
        this.$calendarObj = null
    };

    /* on click on event */
    CalendarApp.prototype.onEventClick =  function (calEvent, jsEvent, view) {
        var $this = this;
        var div = $("<div class='row' style='text-align: center;'></div>");
        div.append("<h4 id='auction-title'>"+calEvent.title+"</h4>");
        console.log(calEvent);
        div.append("<span class='glyphicon glyphicon-time'></span> "+calEvent.startDate+" - " + "<span class='glyphicon glyphicon-time'></span> "+calEvent.endDate);
        div.append("<div><a href='/event/"+calEvent.id+"'>Mais detalhes.</a></div>");
        $this.$modal.modal({
            backdrop: 'static'
        });
        $this.$modal.find('.modal-body').empty().prepend(div);
    },
    /* Initializing */
    CalendarApp.prototype.init = function() {
        var $this = this;
        $this.$calendarObj = $this.$calendar.fullCalendar({
            defaultView: 'month',
            handleWindowResize: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: 'event/calendarJSON',
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar !!!
            eventLimit: true, // allow "more" link when too many events
            selectable: false,
            eventClick: function(calEvent, jsEvent, view) { $this.onEventClick(calEvent, jsEvent, view); },
            timeFormat: 'H:mm'
        });
    },

   //init CalendarApp
    $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp

}(window.jQuery),

//initializing CalendarApp
function($) {
    "use strict";
    $.CalendarApp.init()
}(window.jQuery);
