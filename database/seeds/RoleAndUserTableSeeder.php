<?php

use Illuminate\Database\Seeder;

class RoleAndUserTableSeeder extends Seeder
{

    private function getRandRoles($roles){
        $rolesId = $roles->pluck('id')->toArray();
        $rand = array_rand($rolesId, rand(1, 3));
        if(!is_array($rand)){
            $rand = [$rand];
        }
        $randNumbers = [];
        foreach($rand as $value){
            $randNumbers[] = $rolesId[$value];
        }
        return $randNumbers;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = App\User::where('name', '=', "Admin Ecomp")->count();
        if (!$admin) {
            $admin = App\User::create([
                'name' => "Admin Ecomp",
                'email' => "admin@ecomp.co",
                'password' => '123456',
                'first_login' => 1,
                'ej_id' => DB::table('ejs')->where('name', '=', 'Ecomp')->first()->id
            ]);
        }

        App\Role::firstOrCreate([
            'authorization' => true,
            'photo' => null,
            'name' => 'guest'
        ]);

        App\Role::firstOrCreate([
            'authorization' => true,
            'photo' => null,
            'name' => 'Admin'
        ]);

        if (!$admin) {
            $admin->roles()->sync([2]);
        }

        $roles = factory(App\Role::class, 5)->create();

        $users = factory(App\User::class, 12)->create();

        foreach($users as $user){
            $user->roles()->sync($this->getRandRoles($roles));
        }
    }
}
