<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$faker = Faker\Factory::create();
$faker->addProvider(new Faker\Provider\pt_BR\Person($faker));
$faker->addProvider(new Faker\Provider\pt_BR\Company($faker));
$faker->addProvider(new Faker\Provider\pt_BR\PhoneNumber($faker));
$faker->addProvider(new \App\Helpers\Business($faker));

use Carbon\Carbon;

$factory->define(App\User::class, function () use ($faker) {
    $name = $faker->firstName;
    $username = $faker->unique()->numerify($name.'##');
    return [
        'name' => $name,
        'lastname' => $faker->lastName,
        'email' => $username.'@example.com',
        'username' => $username,
        'password' => '123456',
        'facebook' => '',
        'birthday' => Carbon::createFromFormat('Y-m-d', $faker->date)->format('d/m/Y'),
        'join_date' => Carbon::createFromFormat('Y-m-d', $faker->date)->format('d/m/Y'),
        'remember_token' => str_random(10),
        'ej_id' => App\Ej::all()->random()->id,
    ];
});

$factory->define(App\Role::class, function () use ($faker) {
    return [
        'name' => $faker->unique()->role,
    ];
});

$factory->define(App\Company::class, function () use ($faker) {
    return [
        'name' => $faker->company,
    ];
});

$factory->define(App\Customer::class, function () use ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'telephone' => $faker->phoneNumber,
        'first_contact' => Carbon::createFromFormat('Y-m-d', $faker->date)->format('d/m/Y'),
        'ej_id' => App\Ej::all()->random()->id,
        'address' => $faker->address,
        'job' => $faker->jobTitle,
        'company' => $faker->company,
        'marital_status' => $faker->word,
        'rg' => $faker->randomNumber($nbDigits = 9, $strict = false),
        'cpf' => $faker->randomNumber($nbDigits = 9, $strict = false),
        'cnpj' => $faker->randomNumber($nbDigits = 9, $strict = false),
        'other_contacts' => $faker->sentence($nbWords = 6, $variableNbWords = true)
    ];
});

$factory->define(App\Feedback::class, function () use ($faker) {
    return [
        'name' => $faker->sentence(rand(3, 5)),
        'description' => $faker->text(80),
    ];
});

$factory->define(App\Project::class, function () use ($faker) {

    $deadlineExists = rand(0, 1);
    $finished = rand(0, 1);
    $deadline = $deadlineExists ? Carbon::instance($faker->dateTimeThisYear)->format('d/m/Y') : null;
    $conclusion = $finished ? ($deadlineExists ? Carbon::createFromFormat('d/m/Y', $deadline)->addDays(rand(-20, 55))->format('d/m/Y') : Carbon::instance($faker->dateTimeThisYear)->format('d/m/Y')) : null;

    return [
        'name' => $faker->sentence(rand(3, 5)),
        'description' => $faker->text(80),
        'due_date' => $deadline,
        'conclusion' => $conclusion
    ];
});

$factory->define(App\Department::class, function () use ($faker) {
    return [
        'name' => $faker->unique()->department
    ];
});

$factory->define(App\Timesheet::class, function () use ($faker) {
    return [
        'timesheet_weekday_id' => rand(1, 7),
        'start' => $start = $faker->time,
        'end' => Carbon::createFromFormat('H:i:s', $start)->addHour(1)->format('H:i:s')
    ];
});

$factory->define(App\TimesheetAttendance::class, function () use ($faker) {
    return [
        'datetime' => $faker->dateTimeThisYear->format('d/m/Y H:i'),
        'timesheet_status_id' => App\TimesheetStatus::all()->random(1)->id
    ];
});

$factory->define(App\TimesheetUserSchedule::class, function () use ($faker) {
    return [
    ];
});

$factory->define(App\Event::class, function () use ($faker) {
    $start = Carbon::now()->addDays(rand(-15, 30));
    return [
        'subject' => $faker->sentence(rand(2, 3)),
        'details' => $faker->sentence(rand(3, 7)),
        'start' => $start->format('d/m/Y H:i'),
        'end' => $start->addHour(rand(1, 60))->format('d/m/Y H:i'),
        'user_id' => App\User::all()->random()->id
    ];
});

$factory->define(App\LeaderboardEvent::class, function () use ($faker) {
    return [
        'name' => $faker->city . " Júnior",
        'points' => rand(10, 100),
        'date' => Carbon::instance($faker->dateTimeThisYear)->format('Y-m-d'),
    ];
});
$factory->define(App\Activity::class, function () use ($faker) {
    return [
        'name' => $faker->sentence(rand(3,5)),
        'id_openproject' => rand(100, 9999),
        'hours' => floatval(rand(0, 5) + (rand(0, 99)/100)),
        'type' => rand(0, 1)?'Diretoria':'Projeto',
        'initial_date' => $iDate = Carbon::instance($faker->dateTimeThisYear)->format('Y-m-d'),
        'final_date' => Carbon::createFromFormat('Y-m-d', $iDate)->addDay(rand(1, 14))->format('Y-m-d'),
    ];
});
$factory->define(App\Badge::class, function () use ($faker){
    return [
        'name' => $faker->sentence(rand(1, 3)),
        'points' => rand(50, 300),
    ];
});
$factory->define(App\Ej::class, function () use ($faker){
    return [
        'name' => $faker->company,
        'about' => $faker->sentence(rand(10, 25)),
        'area' => $faker->bs,
        'site' => $faker->url,
        'email' => $faker->safeEmail,
        'state' => $faker->state,
        'city' => $faker->city,
        'address' => $faker->streetAddress,
        'university' => $faker->catchPhrase,
        'majors' => $faker->sentence(rand(2, 5)),
        'facebook' => $faker->url,
        'phone' => $faker->phoneNumber,
    ];
});
$factory->define(App\IndicatorCategory::class, function () use ($faker){
    return [
        'description' => $faker->text,
    ];
});
$factory->define(App\Indicator::class, function () use ($faker){
    return [
        'name' => $faker->sentence($nbWords = 1, $variableNbWords = true),
        'description' => $faker->text,
        'interval_days' => rand (15 , 180),
        'fill_start' => Carbon::createFromFormat('Y-m-d', $faker->date)->format('d/m/Y'),
        'department_id' => App\Department::all()->random()->id,
        'indicator_category_id' => App\IndicatorCategory::all()->random()->id,
        'role_id' => App\Role::all()->random()->id,
    ];
});

$factory->define(App\CustomerEvent::class, function () use ($faker){
    return [
        'type' => $faker->text,
        'place' => $faker->address,
        'outcome' => $faker->text,
        'purpose' => $faker->text,
        'customer_id' => App\Customer::all()->random()->id,
        'datetime' => Carbon::createFromFormat('Y-m-d H:i:s', $faker->date.' 20:30:00')->format('d/m/Y H:i'),
    ];
});

$factory->define(App\Cashflow::class, function () use ($faker){
    $teste = rand(0, 1);
    if ($teste) {
        $str = "Entrada";
    } else {
        $str = "Saída";
    }
    return [
        'value' => rand(10.00, 10000.00),
        'description' =>$faker->text(40),
        'date' =>  Carbon::createFromFormat('Y-m-d', $faker->date)->format('d/m/Y'),
        'movement_type' => $str,    
        'cashflow_category_id' => App\CashflowCategory::all()->random()->id,
        'document' => '/teste/arquivo.pdf',
    ];
});

$factory->define(App\CashflowCategory::class, function () use ($faker){
    return [
        'name' => $faker->text(20),
        'description' => $faker->text(50),
    ];
});

$factory->define(App\GroupCategory::class, function () use ($faker){
    return [
        'name' => $faker->catchPhrase,
    ];
});

$factory->define(App\Group::class, function () use ($faker){
    return [
        'name' => $faker->company,
        'group_category_id' => App\GroupCategory::all()->random()->id,
    ];
});