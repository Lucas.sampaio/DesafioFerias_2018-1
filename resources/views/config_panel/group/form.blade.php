<div class="form-group">
    {!! Form::label('name', 'Nome:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('name', null, ['class' => 'form-control', 'Placeholder' => 'Nome']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('role_list', 'Categorias:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::select('group_category_id', $group_categories, null, ['class' => 'form-control', 'title' => 'Categoria)']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('user_list', 'Membros:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-5">
        {!! Form::select('user_list[]', $users, null, ['class' => 'select2 select2-multiple', 'multiple', 'title' => 'Membros (opcional)']) !!}
    </div>
</div>
@include('errors.validator')