@extends('config_panel.layout')

@section('content')

<div class="page-header">
    <h2>{{ $user->name }}</h2>
</div>

<div class="row">
    <div class="col-md-6">
        <h4>Email: {{ $user->email }}</h4>
        <h4>Telefone: {{ $user->telephone }}</h4>
        <h4>Primeiro contato: {{ $user->first_contact }}</h4>
        @if ($user->projects)
        <h4>Projetos:</h4>
        <ul>
            @foreach($user->projects as $project)
            <li>{{ $project->name }}</li>
            @endforeach
        </ul>
        @endif
    </div>
</div>
@stop