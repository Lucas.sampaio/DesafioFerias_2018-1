<div class=form-group>
    {!! Form::label('name', 'Nome:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-5">
    {!! Form::text('name', null, ['class' => 'form-control', 'Placeholder' => 'Descreva a nova categoria']) !!}
    </div>
</div>
@include('errors.validator')