@extends('config_panel.layout')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Atualizar Categoria de Indicador</h4>
        <ol class="breadcrumb">
            <li>
                <a href="/">Midas</a>
            </li>
            <li>
                <a href="/group/category">Categorias de Indicador</a>
            </li>
            <li class="active">
                Atualizar Categoria de Indicador
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Editar {{ $group_category->description }}</b></h4>
            <div class="row form-horizontal">
                {!! Form::model($group_category, ['route' => ['config.groups.category.update', $group_category->id], 'method' => 'PUT']) !!}
                @include('config_panel.group.group_category.form')
                <div class="col-sm-4"></div>
        	  		<div class="col-sm-8">
                        <button type="submit" class="btn btn-default waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-exclamation"></i>
                            </span>Editar
                        </button>
                        <a type="" class="btn btn-danger waves-effect waves-light" href="admin/group/category">
                            <span class="btn-label"><i class="fa fa-times"></i>
                            </span>Cancelar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
 