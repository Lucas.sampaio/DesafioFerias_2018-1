@extends('config_panel.layout')

@section('plugins-top')
  <!-- Bootstrap table css -->
  <link href="{{ asset('dashboard/plugins/bootstrap-table/css/bootstrap-table.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('dashboard/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"  />

  <!-- Sweet alert -->
  <link href="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
@if (session()->has('flash_message'))
    <div class="row" style="text-align: center;">
        <div class="col-lg-12">
            <div class="panel panel-border panel-{{ session('flash_message_level') }}">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ session('flash_message_title') }}</h3>
                </div>
                <div class="panel-body" style="text-align: center;">
                    <p>
                      {{ session('flash_message') }}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="inline-btn">
            <h4 class="page-title">Cargos</h4>
            <ol class="breadcrumb">
                <li>
                    <a href="/">Midas</a>
                </li>
                <li>
                    <a href="/config">Painel de controle</a>
                </li>
                <li class="active">
                    Cargos
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box">
      <div class="panel-header">
        <h4 class="m-t-0 header-title"><b>Cadastrar cargo</b></h4>
      </div>
      <div class="panel-body">
        <div class="row form-horizontal">
          {!! Form::open(['url' => '/config/job_roles']) !!}
            <div class="form-group">
              {!! Form::label('name', 'Nome:', ['class' => 'col-md-4 control-label']) !!}
              <div class="col-sm-5">
                {!! Form::text('name', null, ['class' => 'form-control col-md-4', 'Placeholder' => 'Nome do cargo']) !!}
                @if ($errors && $errors->has('name'))
                  <span class="help-block">
                      <strong style="color:red;">{{ $errors->first('name') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="text-center">
              {!! Form::button('Criar', ['type' => 'submit', 'class' => 'btn waves-effect waves-light btn-default']) !!}
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box">
      <div class="panel-header">
        <h4 class="m-t-0 header-title"><b>Lista de cargos</b></h4>
      </div>
      <div class="panel-body">
        <div class="col-sm-12">
          <div id="custom-toolbar">
              <button id="delete-row" name="delete" class="btn btn-danger" disabled><i class="fa fa-times m-r-5"></i>Deletar</button>
          </div>
          <table id="custom-table"  data-toggle="table"
             data-toolbar="#custom-toolbar"
             data-search="true"
             data-show-toggle="true"
             data-show-columns="true"
             data-sort-order="desc"
             data-page-list="[5, 10, 20]"
             data-page-size="5"
             data-pagination="true"
             data-show-pagination-switch="true"
             data-locale="pt-BR"
             class="table-bordered ">
             <thead>
                <tr>
                  <th data-field="id" data-align="center" data-checkbox="true"></th>
                  <th data-field="name" data-align="center" data-sortable="true">Nome</th>
                  <th data-field="created_at" data-align="center" data-sortable="true" data-formatter="dateFormatter">Criado em</th>
                  <th data-field="action" data-align="center" data-sortable="true">Ação</th>
                </tr>
            </thead>
            @foreach ($job_roles as $job_role)
              <tr id="{{ $job_role->id }}" data-field="{{ $job_role->id }}">
                <td></td>
                <td>{{ $job_role->name }}</td>
                <td>{{ $job_role->created_at }}</td>
                <td>
                    <form id="edit{{$job_role->id}}" class="form-inline" role="form" method="GET" action="{{ url('/config/job_roles/' . $job_role->id . '/edit') }}">
                        <button class="btn btn-icon waves-effect btn-xs btn-default waves-light" type="submit">
                            Detalhes
                        </button>
                    </form>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('plugins-scripts')
  <script>
    function removeRows(rows) {
      var $table = $('#custom-table'), $remove = $('#delete-row');
      var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
          return row._id
      });

      var temp;
      var undelete = rows.split(",");
      temp = undelete[0].split("[");
      undelete[0] = temp[1];
      temp = undelete[undelete.length-1].split("]");
      undelete[undelete.length-1] = temp[0];

      var i, j;
      for (i = 0; i < ids.length; ++i) {
        for (j = 0; j < undelete.length; ++j) {
          if (undelete[j] == ids[i])
            ids.splice(i, 1);
        }
      }

      console.log(ids);

      $table.bootstrapTable('remove', {
          field: '_id',
          values: ids
      });

      $remove.prop('disabled', true);
    }

    function deleteNotifications() {
      var $deleteArray = [];
      var $non_deleted = [];
      $("input:checkbox[name=btSelectItem]:checked").each(function(){
          $deleteArray.push($(this).parent().parent().attr('data-field'));
      });

      $.ajax({
          url: "/config/job_roles/" + $deleteArray.toString() + "",
          type: "DELETE",
          data: {data : $deleteArray},
          headers: {
              'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
          },
          cache: false,
          success: function($msg){
            removeRows($msg);
          }
      });

      return $non_deleted;
    }

    $(document).ready(function () {
        var $table = $('#custom-table'), $remove = $('#delete-row')

        $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
            $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
            $read.prop('disabled', !$table.bootstrapTable('getSelections').length);
        });

        $remove.click(function () {
            swal({
                title: "Tem certeza que deseja deletar o(s) cargos(s) selecionado(s)?",
                text: "Esses cargos serão deletados permanentemente!",
                type: "error",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger',
                cancelButtonText: "Cancelar",
                confirmButtonText: "Sim, deletar!",
                closeOnConfirm: true,
                allowOutsideClick: true,
            },
            function() {
              deleteNotifications();
            });
        });
    });
  </script>

  <!-- jQuery  -->
  <script src="{{ asset('dashboard/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

  <!-- Bootstrap plugin js -->
  <script src="{{ asset('dashboard/plugins/bootstrap-table/js/bootstrap-table.min.js') }}"></script>
  <script src="{{ asset('dashboard/pages/jquery.bs-table.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/bootstrap-table/locale/bootstrap-table-pt-BR.js') }}"></script>

  <!-- Sweet alert -->
  <script src="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
@endsection
