<!-- FORMULARIO ANTIGO -->
    
    <div class="page-header">
        <h1>Colaboradores</h1>
    </div>
    @if (hasPermission('user.store'))
    <div class="row">
        <div class="col-md-6">
            <div class="page-header">
                <h2>Adicionar um Colaborador</h2>
            </div>
            {!! Form::open() !!}
                @include('user.form', ['text' => ", deixe em branco para gerar e enviar por email ao colaborador"])
                <div class=form-group>
                    {!! Form::submit('Adicionar', ['class' => 'btn btn-success']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <hr>
    @endif
    <table class="table table-hover data-table">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Email</th>
                <th>Cargos</th>
                <th>Horários</th>
                <th>Data que entrou na Empresa</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                {{ $user->roles->
                    filter(function($value) {
                        return $value->name != 'guest';
                    })->implode('name', ', ') }}
            </td>
            <td>
                @foreach($user->timesheet as $timetable)
                {{ $timetable->weekday->name }}: {{ $timetable->start }} - {{ $timetable->end }} <br>
                @endforeach
            </td>
            <td>{{ $user->join_date }}</td>
            <td>
            @if (hasPermission('user.destroy'))
            {!! Form::open(['route' => ['user.destroy', $user->id], 'method' => 'delete']) !!}
                {!! Form::submit('Apagar', ['class'=>'btn btn-xs btn-danger']) !!}
            {!! Form::close() !!}
            @endif
            </td>
            <td>
            @if (hasPermission('user.edit'))
            {!! Form::open(['route' => ['user.edit', $user->id], 'method' => 'get']) !!}
                {!! Form::submit('Editar', ['class'=>'btn btn-xs btn-default']) !!}
            {!! Form::close() !!}
            @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    !-->