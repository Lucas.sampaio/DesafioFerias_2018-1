@extends('config_panel.layout')

@section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="inline-btn">
        	<h4 class="page-title">Empresas juniores</h4>
        	<ol class="breadcrumb">
        		<li>
        			<a href="/">Midas</a>
        		</li>
        		<li>
        			<a href="/ejs">Empresas juniores</a>
        		</li>
        		<li class="active">
        			{{ $ej->name }}
        		</li>
        	</ol>
        </div>
        <div class="btn-group pull-right m-t-15">
            <a type="button" class="btn btn-success waves-effect waves-light" aria-expanded="false" href="/ejs/{{ $ej->id }}/edit">Editar <span class="m-l-5"><i class="md md-mode-edit"></i></span></a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="profile-detail card-box">
            <div>
				@if ($ej->image)
		        	<img class="img-circle" src="{!!URL::to('/') . '/img/ejs/' . $ej->image!!}" alt="profile-image">
				@else
					<img class="img-circle" src="{!!URL::to('/') . '/img/ejs/ejstock.png'!!}" alt="profile-image">
				@endif

                <ul class="list-inline status-list m-t-20">
                    <li>
                        <h3 class="text-primary m-b-5">456</h3>
                        <p class="text-muted">Followings</p>
                    </li>
    
                    <li>
                        <h3 class="text-success m-b-5">5864</h3>
                        <p class="text-muted">Followers</p>
                    </li>
                </ul>
    
                <button type="button" class="btn btn-pink btn-custom btn-rounded waves-effect waves-light">Follow</button>
    
                <hr>
                <h4 class="text-uppercase font-600">Sobre</h4>
    
                <p class="text-muted font-13 m-b-30">
                    {{ $ej->about }}
                </p>
    
                <div>
                    <p class="text-muted font-13"><strong>Nome :</strong> <span class="m-l-15">{{ $ej->name }}</span></p>
    
                    <p class="text-muted font-13"><strong>Telefone :</strong><span class="m-l-15">{{ $ej->phone }}</span></p>
    
                    <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{ $ej->email }}</span></p>
    
                    <p class="text-muted font-13"><strong>Endereco :</strong> <span class="m-l-15">{{ $ej->address }}</span></p>
    
                    <p class="text-muted font-13"><strong>Cidade :</strong> <span class="m-l-15">{{ $ej->city }}</span></p>
    
                    <p class="text-muted font-13"><strong>Estado :</strong> <span class="m-l-15">{{ $ej->state }}</span></p>
    
                    <p class="text-muted font-13"><strong>Faculdade :</strong> <span class="m-l-15">{{ $ej->university }}</span></p>
    
                    <p class="text-muted font-13"><strong>Area de Atuacao :</strong> <span class="m-l-15">{{ $ej->area }}</span></p>
    
                    <p class="text-muted font-13"><strong>Site :</strong> <span class="m-l-15"><a href="{{ $ej->site }}">{{ $ej->site }}</a></span></p>
    
                    <p class="text-muted font-13"><strong>Cursos :</strong> <span class="m-l-15">{{ $ej->majors }}</span></p>
    
                </div>
    
    
                <div class="button-list m-t-20">
                    <button type="button" class="btn btn-facebook waves-effect waves-light">
                       <i class="fa fa-facebook"></i>
                    </button>
    
                    <button type="button" class="btn btn-twitter waves-effect waves-light">
                       <i class="fa fa-twitter"></i>
                    </button>
    
                    <button type="button" class="btn btn-linkedin waves-effect waves-light">
                       <i class="fa fa-linkedin"></i>
                    </button>
                    
                    <button type="button" class="btn btn-dribbble waves-effect waves-light">
                       <i class="fa fa-envelope"></i>
                    </button>

                    <!-- <button type="button" class="btn btn-dribbble waves-effect waves-light">
                       <i class="fa fa-dribbble"></i>
                    </button> -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
