@extends('auth.alayout')

@section('content')
<div class=" card-box">
	<div class="panel-heading">
		<h3 class="text-center"> Editar <strong class="text-custom">{{ $ej->name }}</strong> </h3>
	</div>

	<div class="panel-body">
		<div>
			@if ($errors->any())
				<ul class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
		</div>
		<form class="form-horizontal m-t-20" role="form" method="POST" action="/ejs/{{ $ej->id }}" enctype="multipart/form-data">
            {!! csrf_field() !!}

			<input type="hidden" name="_method" value="PATCH">
			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Nome :</strong><input class="form-control" type="text" required="" placeholder="Nome" name="name" value="{{ $ej->name }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Email :</strong><input class="form-control" type="email" required="" placeholder="Email" name="email" value="{{ $ej->email }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Telefone :</strong><input class="form-control" type="tel" data-mask="(99) 99999-9999" placeholder="Telefone" name="phone" value="{{ $ej->phone }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Endereco :</strong><input class="form-control" type="text" required="" placeholder="Endereco" name="address" value="{{ $ej->address }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Cidade :</strong><input class="form-control" type="text" required="" placeholder="Cidade" name="city" value="{{ $ej->city }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Estado :</strong><input class="form-control" type="text" required="" placeholder="Estado" name="state" value="{{ $ej->state }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Faculdade :</strong><input class="form-control" type="text" required="" placeholder="Faculdade" name="university" value="{{ $ej->university }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Area de Atuacao :</strong><input class="form-control" type="text" required="" placeholder="Area de Atuacao" name="area" value="{{ $ej->area }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Site :</strong><input class="form-control" type="text" placeholder="Site" name="site" value="{{ $ej->site }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Cursos :</strong><input class="form-control" type="text" placeholder="Cursos" name="majors" value="{{ $ej->majors }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Facebook :</strong><input class="form-control" type="text" placeholder="Facebook" name="facebook" value="{{ $ej->facebook }}">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<strong>Sobre :</strong><textarea class="form-control" type="textarea" placeholder="Sobre" name="about">{{ $ej->about }}</textarea>
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<div class="m-b-0">
						@if ($ej->image)
							<p><label class="control-label">Imagem Atual:</label></p>
							<div class="profile-detail rm-padding">
                        		<img class="img-circle" src="{!!URL::to('/') . '/img/ejs/' . $ej->image!!}">
                        	</div>
							<p><label class="control-label">Trocar imagem:</label></p>
						@else
							<label class="control-label">Imagem:</label>
						@endif
						<input class="filestyle" data-buttonname="btn-primary" type="file" name="ej_image">
					</div>
				</div>
			</div>

			<div class="form-group text-center m-t-40">
				<div class="col-xs-12">
					<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
						Enviar
					</button>
				</div>
			</div>

		</form>

	</div>
</div>

@endsection

@section('plugins-scripts')

    <script src="{{ asset('dashboard/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/pages/jquery.form-advanced.init.js')}}" type="text/javascript"></script>

@endsection
