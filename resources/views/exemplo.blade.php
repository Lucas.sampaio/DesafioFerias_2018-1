@extends('layout')
@section('plugins-top')
    <link href="dashboard/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
@stop
@section('page-title')
	<h4 class="page-title">Exemplo de Título da Página</h4>
	{{-- Breadcrumb --}}
	<ol class="breadcrumb">
	    <li>
	        <a href="#">Ubold</a>
	    </li>
	    <li>
	        <a href="#">Apps</a>
	    </li>
	    <li class="active">
	        Taskboard
	    </li>
	</ol>
@stop
@section('content')
	<h1>Conteudo da página</h1>
@stop
@section('plugins-bottom')
	<script src="dashboard/plugins/jquery-ui/jquery-ui.min.js"></script>
@stop
