@extends('layout')
@section('content')
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="">
      <div class="container-fluid text-center">
        <h1>Frase inspiradora do dia: </h2>
        <blockquote>{{ $inspire }}</blockquote>
        <p style='font-size:6px; color:#aaa'></p>
        <!--<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>-->
      </div>
    </div>

@stop