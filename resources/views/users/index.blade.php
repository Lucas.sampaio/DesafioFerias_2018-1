@extends('layout')

@section('css')


@endsection

@section('plugins-top')

    
@stop

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Membros</h4>
        <ol class="breadcrumb">
            <li>
                <a href="/">Midas</a>
            </li>
            <li class="active">
                Membros
            </li>
        </ol>
    </div>
</div>

<div class="row">
	<div class="col-sm-8">
		<form role="form">
            <div class="form-group contact-search m-b-30">
            	<input type="text" id="search" onkeyup="Search()" class="form-control" placeholder="Buscar...">
                <button type="submit" class="btn btn-white"><i class="fa fa-search"></i></button>
            </div>
        </form>
	</div>
</div>

<!-- Page-Title -->
                        
<ul class="row" id="user-list">
    @foreach ($users as $user)
	<li class="col-sm-6 col-lg-6">
		<div class="card-box" id="user" style="height: 135px;">
			<div class="contact-card">
				<a class="pull-left" href="/user/{{ $user->id }}">
                    <!--<img style="margin-left = 50px;" class="img-circle" src="{{ asset('img/users/userstock.png') }}"alt="">-->
                    @if($user->image!=null)
                        <img class="img-circle" style="margin-left: 20px;" src="img/users/{{  $user->image }}"></img>
                    @else
                        <img style="margin-left: 20px;" class="img-circle" src="{{ asset('img/users/userstock.png') }}"alt="">
                    @endif
                </a>
                <div class="member-info" style="margin-left: 20px;">
                    <h4 class="m-t-0 m-b-5 header-title"><b id="user-name"><a href="/user/{{ $user->id }}">{{ $user->name }} {{ $user->lastname }}</a></b></h4>
                    <p class="text-muted">{{  $user->roles->filter(function($value) {return $value->name != 'guest';} )->implode('name', ', ') }}</p>
                    <p class="text-dark"><i class="md  md-email m-r-10"></i><small>{{  $user->email }}</small></p>
                </div>
                <div class="contact-action">
                    <a href="" ><i class="ion-social-facebook-outline"></i></a>
                    {{--<form id="" role="form" action="" method="POST" class="inline-btn">
                        {{ csrf_field() }}  
                        {{ method_field('DELETE') }}
                        <input type="hidden">
                        <button type="submit" class="btn btn-danger btn-sm" value=""><i class="md md-close"></i></button>
                    </form> --}}
                </div>
			</div>
		</div>
    </li> <!-- end col -->
    @endforeach
    
</ul>

@endsection

@section('plugins-scripts')
<script>
function Search() {
    // Declare variables
    var input, filter, ul, li, a, i;
    input = document.getElementById('search');
    filter = input.value.toUpperCase();
    ul = document.getElementById("user-list");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        console.log(li.length);
        a = li[i].getElementsByTagName("b")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
</script>
@endsection
