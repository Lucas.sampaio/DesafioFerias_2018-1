<div class=form-group>
    {!! Form::label('name', 'Nome:', ['class' => 'sr-only']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'Placeholder' => 'Nome']) !!}
</div>
@include('errors.validator')