@extends('layout')

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="page-header">
            <h2>Atualizar Empresa</h2>
        </div>
        {!! Form::model($company, ['action' => ['CRM\CompanyController@update', $company->id], 'method' => 'PUT']) !!}
        @include('company.form')
        <div class=form-group>
            {!! Form::submit('Atualizar', ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>
@stop