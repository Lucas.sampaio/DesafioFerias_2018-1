@extends('layout')

@section('css')

    <link href="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css">

@endsection

@section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-8">
    	<h4 class="page-title">Empresas juniores</h4>
    	<ol class="breadcrumb">
    		<li>
    			<a href="/">Midas</a>
    		</li>
    		<li class="active">
    			Empresas juniores
    		</li>
    	</ol>
    </div>
    <div class="col-sm-4 pull-right">
		 <a href="/ejs/create" class="btn btn-default btn-md waves-effect waves-light m-b-30 pull-right"><i class="md md-add"></i> Registrar Ej</a>
	</div>
</div>


<div class="row">
    @foreach($ejs as $ej)
        <div class="col-sm-6 col-lg-4">
            <div class="card-box">
                <div class="contact-card">
                    <a class="pull-left" href="#">
						@if ($ej->image)
                        	<img class="img-circle" src="{!!URL::to('/') . '/img/ejs/' . $ej->image!!}" alt="profile-image">
						@else
							<img class="img-circle" src="{!!URL::to('/') . '/img/ejs/ejstock.png'!!}" alt="profile-image">
						@endif
                    </a>
                    <div class="member-info">
                        <h4 class="m-t-0 m-b-5 header-title"><b>{{ $ej->name }}</b></h4>
                        <p class="text-muted">{{ $ej->area }}</p>
                        <p class="text-dark"><i class="md md-school m-r-10"></i><small>{{ $ej->university }}</small></p>
                        <div class="contact-action">
                            <a href="/ejs/{{ $ej->id }}" class="btn btn-info btn-sm"><i class="icon-question"></i></a>
                            {{--<form id="delete{{ $ej->id }}" role="form" action="/ejs/{{ $ej->id }}" method="POST" class="inline-btn">
                                {{ csrf_field() }}  
                                {{ method_field('DELETE') }}
                                <input type="hidden">
                                <button type="submit" class="btn btn-danger btn-sm" value=""><i class="md md-close"></i></button>
                            </form> --}}
                        </div>
                    </div>

                </div>
            </div>
        </div> <!-- end col -->
    @endforeach
</div>

@endsection

@section('plugins-scripts')

    <script src="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/pages/jquery.sweet-alert.init.js')}}" type="text/javascript"></script>

@foreach ($ejs as $ej)
    <script>
      $('#delete{{$ej->id}}').click(function(event) {
          event.preventDefault();
          swal({
            title: "Tem certeza que deseja deletar a Ej {!! $ej->name !!}?",
            text: "A Empresa júnior será apagada permanentemente do sistema e não poderá ser recuperada.",
            type: "error",
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sim, deletar!",
            closeOnConfirm: false,
            allowOutsideClick: true,
          },
          function(){
            $('#delete{{$ej->id}}').submit();
          });
      });
    </script>
@endforeach

@endsection
