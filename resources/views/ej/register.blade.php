@extends('auth.alayout')

@section('content')
<div class=" card-box">
	<div class="panel-heading">
		<h3 class="text-center"> Registrar <strong class="text-custom">Empresa Júnior</strong> </h3>
	</div>

	<div class="panel-body">
		<div>
			@if ($errors->any())
				<ul class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
		</div>
		<form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/ejs/') }}" enctype="multipart/form-data">
            {!! csrf_field() !!}

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" required="" placeholder="Nome" name="name">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="email" required="" placeholder="Email" name="email">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="tel" data-mask="(99) 99999-9999" placeholder="Telefone" name="phone">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" required="" placeholder="Endereco" name="address">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" required="" placeholder="Cidade" name="city">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" required="" placeholder="Estado" name="state">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" required="" placeholder="Faculdade" name="university">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" required="" placeholder="Area de Atuacao" name="area">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" placeholder="Site" name="site">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" placeholder="Cursos" name="majors">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" placeholder="Facebook" name="facebook">
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<textarea class="form-control" type="textarea" placeholder="Sobre" name="about"></textarea>
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<div class="m-b-0">
						<label class="control-label">Imagem:</label>
						<input class="filestyle" data-buttonname="btn-primary" data-buttonText="Escolher" type="file" name="ej_image">
					</div>
				</div>
			</div>

			<div class="form-group text-center m-t-40">
				<div class="col-xs-12">
					<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
						Registrar
					</button>
				</div>
			</div>

		</form>

	</div>
</div>

@endsection

@section('plugins-scripts')

    <script src="{{ asset('dashboard/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/pages/jquery.form-advanced.init.js')}}" type="text/javascript"></script>

@endsection

@section('js')

@endsection
