@extends('auth.alayout')

@section('content')
<div class=" card-box">
  <div class="panel-heading">
      <h3 class="text-center"> Entrar no <strong class="text-custom">Midas</strong> </h3>
  </div>
  <div class="panel-body">
    @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
    @endif
    <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="col-xs-12">
                <input class="form-control" type="email" required="" placeholder="Email" name="email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="col-xs-12">
                <input class="form-control" type="password" required="" placeholder="Senha" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                <div class="checkbox checkbox-primary">
                    <input id="checkbox-signup" type="checkbox" name="remember">
                    <label for="checkbox-signup">
                        Lembre-me
                    </label>
                </div>

            </div>
        </div>
        <div class="form-group text-center m-t-40">
            <div class="col-xs-12">
                <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Entrar</button>
            </div>
        </div>
        <div class="form-group m-t-30 m-b-0">
            <div class="col-sm-12">
                <a href="{{ url('/password/reset') }}"><i class="fa fa-lock m-r-5"></i> Esqueceu sua senha?</a>
            </div>
        </div>
    </form>
  </div>
</div>
<div class="row">
  <div class="col-sm-12 text-center">
		<p>Não tem uma conta? <a href="{{ url('/register') }}" class="text-primary m-l-5"><b>Registrar</b></a></p>
  </div>
</div>
@endsection
