<div class="form-group">
    {!! Form::label('name', 'Nome:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('name', null, ['class' => 'form-control', 'Placeholder' => 'Nome']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('lastname', 'Sobrenome:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('lastname', null, ['class' => 'form-control', 'Placeholder' => 'Sobrenome']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('user_name', 'Usuário:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('user_name', null, ['class' => 'form-control', 'Placeholder' => 'Usuário']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('email', 'Email:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('email', null, ['class' => 'form-control', 'Placeholder' => 'Email']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::text('facebook', null, ['class' => 'form-control', 'Placeholder' => 'Facebook']) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('password_hack', 'Senha:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
    	{!! Form::password('password_hack', ['class' => 'form-control', 'Placeholder' => $text]) !!}
	</div>
</div>
<div class="form-group">
    {!! Form::label('birthday', 'Data de nascimento:', ['class' => 'col-md-4 control-label']) !!}
	<div class="col-md-5">
        <div class="input-group">
        	{!! Form::text('birthday', null, ['class' => 'form-control date datepicker-pt-br', 'Placeholder' => 'dd/mm/aaaa', 'id' => 'datepicker-autoclose', 'data-mask' => '99/99/9999']) !!}
            <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
        </div>
	</div>
</div>
<div class="form-group">
    {!! Form::label('image', 'Foto', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-5">
        <input class="filestyle" data-buttonname="btn-primary" data-buttonText="Escolher Imagem" type="file" name="image">
    </div>
</div>
@section('plugins-scripts')
    <script src="{{ asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dashboard/js/bootstrap-timepicker.js')}}"></script>
 	<script src="{{ asset('dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/datapickerpt-br.js')}}"></script>
    
@stop

@include('errors.validator')