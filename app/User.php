<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;

use App\Notifications\UserResetPasswordNotification;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'join_date'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'lastname', 'username', 'email', 'password', 'birthday', 'join_date', 'exit_date', 'image', 'password_hack', 'ej_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * Relationships
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    public function customerEvents()
    {
        return $this->belongsToMany('App\CustomerEvent')->withTimestamps();
    }

    public function ej()
    {
        return $this->belongsTo("App\Ej", "ej_id");
    }

    public function filled_indicators()
    {
        return $this->hasMany('App\IndicatorFill');
    }

    public function events()
    {
        return $this->belongsToMany('App\Event')->withTimestamps();
    }

    public function addedEvents()
    {
        return $this->hasMany('App\Event');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity', 'user_id');
    }

    public function leaderboardEvents()
    {
        return $this->belongsToMany('App\LeaderboardEvent');
    }

    public function badges()
    {
        return $this->belongsToMany('App\Badge')->withTimestamps();
    }

    // public function attendances()
    // {
    //     return $this->hasManyThrough('App\TimesheetAttendance', 'App\TimesheetUserSchedule', 'user_id', 'timesheet_user_schedule_id');
    // }

    /**
     * Query scopes
    */
    public function scopeNoAdmin($query)
    {
        return $query->where('id', '!=', '1')->get();
    }

    public function scopeAlphabeticalOrder($query)
    {
        return $query->orderBy('name');
    }

    /**
     * Accessors
    */
    public function setJoinDateAttribute($value)
    {
        $this->attributes['join_date'] = empty($value) ? null : Carbon::createFromFormat('d/m/Y', $value);
    }

    public function getJoinDateAttribute($value)
    {
        return empty($value) ? "" : Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }
    
    public function getBirthdayAttribute($value)
    {
        return empty($value) ? "" : Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }
    
    public function getExitDateAttribute($value)
    {
        return empty($value) ? "" : Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
    }
    
    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = empty($value) ? null : Carbon::createFromFormat('d/m/Y', $value);
    }
    
    public function setExitDateAttribute($value)
    {
        $this->attributes['exit_date'] = empty($value) ? null : Carbon::createFromFormat('d/m/Y', $value);
    }
    
    public function getRoleListAttribute()
    {
        return $this->roles->pluck('id')->toArray();
    }

    public function getTimesheetListAttribute()
    {
        return $this->timesheet->pluck('id')->toArray();
    }

    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }
    public function setPasswordHackAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }

    public function getAttendancesAttribute()
    {
        $attendances = [];
        foreach($this->timesheetSchedule as $timetable){
            foreach($timetable->attendances as $attendance){
                $attendances[] = $attendance;
            }
        }
        return collect($attendances)->sortByDesc('datetimeCarbon');
    }

    public function getLatestAttendancesAttribute()
    {
        $attendances = [];
        foreach($this->timesheetSchedule as $timetable){
            foreach($timetable->attendances as $attendance){
                $attendances[] = $attendance;
            }
        }
        return collect($attendances)->sortByDesc('datetimeCarbon');
    }
    public function getNotRescheduleAttendancesAttribute()
    {
        $attendances = $this->latestAttendances->take(10);
        return $attendances->reject(function($attendance){
            return !is_null($attendance->parentAttendance);
        });
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group')->withPivot('job_role_id')->withTimestamps();
    }

    public function tasks()
    {
        return $this->belongsToMany('App\TaskboardTask', 'tasks_users', 'task_id', 'user_id');
    }

    public function attendanceUserHour()
    {
        return $this->hasMany('App\AttendanceUserHour');
    }
    
    public function attendanceUserTime()
    {
        return $this->hasMany('App\AttendanceUserTime');
    }
    
    public function attendanceUserAttendance()
    {
        return $this->hasMany('App\AttendanceUserAttendance');
    }

    public function getGroupListAttribute()
    {
        return $this->groups->pluck('id')->toArray();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
    }
}
