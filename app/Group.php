<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name', 'group_category_id'];

    /**
     * Relationships
    */
    public function category()
    {
        return $this->belongsTo('App\GroupCategory', 'group_category_id');
    }

    public function users()
    {
    	return $this->belongsToMany('App\User')->withPivot('job_role_id')->withTimestamps();
    }

    public function getUserListAttribute()
    {
        return $this->users->pluck('id')->toArray();
    }
    
    public function taskboards()
    {
        return $this->belongsToMany('App\Taskboard', 'taskboards_groups');
    }
}
