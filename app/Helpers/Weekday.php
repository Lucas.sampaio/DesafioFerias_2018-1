<?php

namespace App\Helpers;

class Weekday extends \Faker\Provider\Base
{
    public function weekday($index)
    {
        $weekdays = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];
        return $weekdays[$index];
    }
}
