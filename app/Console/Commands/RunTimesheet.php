<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\User;
use App\TimesheetUserSchedule;
use App\TimesheetAttendance;

class RunTimesheet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timesheet:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria os horários dos usuários';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now(new \DateTimeZone('America/Sao_Paulo')); //the server may be located somewhere else

        $schedules = TimesheetUserSchedule::all(); //all schedules
        
        foreach($schedules as $schedule){
            $timesheet = $schedule->timesheet;
            if($timesheet->weekday->id == $now->dayOfWeek){ //same weekday
                $date = Carbon::createFromFormat('H:i', $timesheet->start); //create a date using schedule start time
                if(($date->diffInMinutes($now) + $now->offsetHours*60) < 0 && ($date->diffInMinutes($now) + $now->offsetHours*60) > -30){ 
                    //30m before schedule
                    $attendances = $schedule->attendances;
                    $alreadyCreated = false; //create attendance only if not already created in this week
                    foreach($attendances as $attendance){
                        if(abs(Carbon::createFromFormat('d/m/Y H:i', $attendance->datetime)->diffInDays($now)) <= 6){
                            $alreadyCreated = true;
                        }
                    }
                    if(!$alreadyCreated){
                        $this->info('Criando horário...' . $date . '. Para usuário ' . $schedule->user->name);
                        
                        $attendance = $schedule->attendances()->create([
                            'datetime' => $date->format('d/m/Y H:i'), //schedule start time
                            'timesheet_status_id' => 1
                        ]);
                        \Log::info('Criando horário...' . $date . '. Para usuário ' . $schedule->user->name);
                    }
                   
                }
            }
        }
        
        
        
    }
}
