<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ej extends Model
{
    protected $fillable = [
        'name', 
        'about', 
        'area', 
        'site', 
        'email', 
        'state', 
        'city', 
        'address', 
        'university', 
        'majors', 
        'facebook', 
        'phone',
		'image'
    ];
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function projects()
    {
        return $this->hasMany('App\User');
    }

    public function customers()
    {
        return $this->hasMany('App\Customer');
    }

}
