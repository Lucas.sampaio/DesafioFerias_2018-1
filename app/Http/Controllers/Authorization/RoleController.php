<?php

namespace App\Http\Controllers\Authorization;

use Illuminate\Http\Request;
use App\Role;
use App\Http\Requests;
use App\Http\Requests\RoleRequest;
use App\Http\Controllers\Controller;
use Route;
use App\Permission;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $routes = $this->getRoutes();
        
        return view('role.index', [
            'roles' => $roles,
            'routes' => $routes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $request['authorization'] = true;

        $role = Role::create($request->all());
        
        $this->storePermissions($request->input('route_list'), $role);
        return redirect('admin/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $routes = $this->getRoutes();
        
        return view('role.edit', compact('role', 'routes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->update($request->all());
        
        Permission::where('role_id', '=', $id)->delete();
        
        $this->storePermissions($request->input('route_list'), $role);
        return redirect('admin/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return redirect('admin/role');
    }
    
    private function getRoutes()
    {
        $routes = (Route::getRoutes());
        
        $newRoutes = [];
        foreach($routes as $route){
            //TODO: force route name
            /*if($route->getName() != null){
                $name = $route->getName();
            } else {
                $name = $route->getActionName();
            }*/
            $name = $route->getName();
            
            $newRoutes[$name] = $name;
        }
        return collect($newRoutes)->except("");
    }
    
    private function storePermissions($routes, Role $role)
    {
        if(is_null($routes)){
            return;
        }
        foreach($routes as $route){
            Permission::create([
                'role_id' => $role->id,
                'route' => $route,
            ]);
        }
    }
}
