<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Ej;
use App\Group;
use Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::alphabeticalOrder()->get();
        $roles = Role::noGuest()->pluck('name', 'id');
        $groups = Group::pluck('name', 'id');
        $ejs = Ej::pluck('name', 'id')->toArray();

        return view('config_panel.user.index', compact('users', 'roles', 'ejs', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $faker = \Faker\Factory::create('pt_BR');
        $fields = $request->all();

        if(empty($request->input('password'))){
            $fields['password'] = str_random(10);
            //$fields = (object) $fields;
        }
        
        $username = $faker->unique()->numerify($request->input('name').'##');
        if(empty($request->input('username'))){
            $fields['username'] = $username;
            $fields = (object) $fields;
        }
        
        $user = User::create((array) $fields);

        //$user->groups()->sync($request->input('group_list'));
        
        if ($image = $request->file('image')) {
		    if(($image->isValid()? substr($image->getMimeType(), 0, 5): null) == 'image') {
    			$destinationPath = base_path() . '/public/img/users/';
    			$user->image = $user->id . '.' . $image->getClientOriginalExtension();
    			$image->move($destinationPath, $user->image);
            	$user->save();
		    }
        }
        return redirect('config/users');
    }
    
    /* Display all users on the page user */
    public function showAll()
    {
        $users = User::alphabeticalOrder()->get();
        $roles = Role::noGuest()->pluck('name', 'id');
        $groups = Group::pluck('name', 'id');
        $ejs = Ej::pluck('name', 'id')->toArray();

        return view('users.index', compact('users', 'roles', 'ejs', 'groups'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $user = User::findOrFail($id);
       return view('config_panel.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::findOrFail($id);
        //$roles = Role::noGuest()->lists('name', 'id');
        //$groups = Group::lists('name', 'id');
        //$ejs = Ej::lists('name', 'id')->toArray();

        return view('config_panel.user.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
       if(empty($request->input('password_hack'))){
            $user->update($request->except('password_hack', 'image'));
        } else {
            $user->update($request->except('image'));
        }

        if ($image = $request->file('image')) {
		    if(($image->isValid()? substr($image->getMimeType(), 0, 5): null) == 'image') {
    			$destinationPath = base_path() . '/public/img/users/';
    			/*if (file_exists($destinationPath . $user->image)) {
    			    unlink($destinationPath . $user->image);
		        }*/
    			$user->image = $user->id . '.' . $image->getClientOriginalExtension();
    			$image->move($destinationPath, $user->image);
    			$user->save();
		    }
        }
        
 
        
        return redirect('config/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('config/users');
    }

    /**
     * Private Methods
     */
    private function syncRoles($request, User $user)
    {
        $roles = collect($request->input('role_list'));
        if(!$roles->contains(Role::guest()->id)){
            $roles->push(Role::guest()->id);
        }
        $user->roles()->sync($roles->toArray());
    }
}
