<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use Validator;
use App\User;
use App\JobRole;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class JobRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $job_roles = JobRole::get()->all();
      return view('config_panel.job_role.index')->with([
        'job_roles' => $job_roles
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $job_role_exist = JobRole::where('name', $request->name)->get()->first();

      $rules = [
        'name' => 'required|max:80',
      ];
      $messages = [
          'name.required' => 'O campo "nome" não pode estar vazio!',
          'name.max' => 'O campo "nome" deve possuir no máximo 80 caracteres!'
      ];

      $validation = Validator::make($request->all(), $rules, $messages);

      if ($validation->fails()) {
        return redirect('/config/job_roles')->with([
            'errors' => $validation->errors(),
        ]);
      } else if ($job_role_exist) {
        //flash message
        return redirect('/config/job_roles');
      } else {
        $job_role = JobRole::create([
          'name' => $request->name
        ]);
        $job_role->save();
      }

      return redirect('/config/job_roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job_role = JobRole::where('id', $id)->get()->first();
        $users = User::get();

        return view('config_panel.job_role.show')->with([
          'job_role' => $job_role,
          'users' => $users
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $job_role = JobRole::where('id', $id)->get()->first();
      $job_role_exist = JobRole::where('name', $request->name)->get()->first();

      $rules = [
        'name' => 'required|max:80',
      ];
      $messages = [
          'name.required' => 'O campo "nome" não pode estar vazio!',
          'name.max' => 'O campo "nome" deve possuir no máximo 80 caracteres!'
      ];

      $validation = Validator::make($request->all(), $rules, $messages);

      if ($validation->fails()) {
        return redirect('/config/job_roles/' . $job_role->id . '/edit')->with([
          'job_role' => $job_role,
          'errors' => $validation->errors()
        ]);
      } else if ($job_role_exist) {
        //flash message
        return redirect('/config/job_roles/' . $job_role->id . '/edit')->with([
          'job_role' => $job_role,
        ]);
      } else {
        $job_role->update([
          'name' => $request->name
        ]);
        $job_role->save();
      }

      return redirect('/config/job_roles/' . $job_role->id . '/edit')->with([
          'job_role' => $job_role
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $job_roles = JobRole::whereIn('id', $request->data)->get();
      $users = User::get();

      $non_deleted = array();

      foreach ($job_roles as $job_role) {
        foreach ($users as $user) {
          if ($user->groups()->wherePivot('job_role_id', $job_role->id)->get()->all()) {
            array_push($non_deleted, $job_role->id);
            break;
          }
        }
        if (!in_array($job_role->id, $non_deleted)) $job_role->forceDelete();
      }

      return json_encode($non_deleted);
    }
}
