<?php

namespace App\Http\Controllers\ConfigPanel\Schedule;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BaseSchedule;
use App\BaseTime;
use App\PluginControlTable;
use App\Weekday;
use DB;
use Validator;

use App\Http\Requests\BaseScheduleRequest;

class BaseScheduleController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $week_days = Weekday::get()->pluck('name')->all();

    foreach ($week_days as $key => $weekday) {
      if ($weekday == "sunday") $week_days[$key] = "domingo";
      if ($weekday == "monday") $week_days[$key] = "segunda";
      if ($weekday == "tuesday") $week_days[$key] = "terça";
      if ($weekday == "wednesday") $week_days[$key] = "quarta";
      if ($weekday == "thursday") $week_days[$key] = "quinta";
      if ($weekday == "friday") $week_days[$key] = "sexta";
      if ($weekday == "saturday") $week_days[$key] = "sábado";
    }

    $tables = BaseSchedule::get()->all();

    return view('config_panel.schedule.index')->with([
     'week_days' => $week_days,
     'tables' => $tables,
     'return' => session()->has('return') ? session('return') : null
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  public function storeBaseSchedule(BaseScheduleRequest $request)
  {
    BaseSchedule::create($request->all());

    flash('Itinerário criado com sucesso!', 'success');
    return redirect()->route('config.schedule.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // dd($request);
    //Validation first
    $rules = [
      'name' => 'required|max:80',
      'open_time' => 'required',
      'close_time' => 'required',
      'week_days' => 'required',
      'slice' => 'required',
    ];
    $messages = [
        'name.required' => 'O campo "nome" não pode estar vazio!',
        'name.max' => 'O campo "nome" deve possuir no máximo 80 caracteres!',
        'open_time.required' => 'O campo "Abertura" não pode estar vazio!',
        'close_time.required' => 'O campo "Fechamento" não pode estar vazio!',
        'week_days.required' => 'O campo "Dias da semana" não pode estar vazio!',
        'slice.required' => 'O campo "Fatia" não pode estar vazio!'
    ];

    $validation = Validator::make($request->all(), $rules, $messages);

    $time = explode(":", $request->open_time);
    if (count($time) == 2) {
      $minutes_init = intval($time[0])*60 + intval($time[1]);
      $time = explode(":", $request->close_time);
      $minutes_end = intval($time[0])*60 + intval($time[1]);

      if ($minutes_end <= $minutes_init)
        $validation->errors()->add('minutes_end', '"Fechamento" deve ser menor que "Abertura"');

      $slice_to_minutes = explode(":", $request->slice);
      $slice_to_minutes = intval($slice_to_minutes[0])*60 + intval($slice_to_minutes[1]);

      if (($minutes_end - $minutes_init) % $slice_to_minutes)
        $validation->errors()->add('slice', '"Fatias" não cabem entre "Abertura" e "Fechamento"!');
    }

    if ($validation->errors()->messages()) {
      return redirect('/config/schedules/base_schedule')->with([
          'errors' => $validation->errors(),
          'return' => $request->all()
      ]);
    }

    $base_schedule = BaseSchedule::create([
      'name' => $request->name,
      'open_time' => $request->open_time,
      'close_time' => $request->close_time,
      'slice' => $request->slice,
      'is_active' => (count(BaseSchedule::get()) ? false : true)
    ]);

    $base_schedule->save();

    foreach ($request->week_days as $week_day) {
      $weekday = Weekday::where('id', $week_day+1)->get()->first();
      $base_schedule->weekdaysSchedule()->save($weekday);
    }

    foreach ($request->week_days as $week_day) {
      $weekday = Weekday::where('id', $week_day+1)->get()->first();
      for ($i = $minutes_init; $i < $minutes_end; $i+=$slice_to_minutes) {
        $hours = floor($i/60);
        $minutes = $i - ($hours * 60);
        $start = ($hours < 10 ? ('0' + $hours) : $hours) . ':' . ($minutes < 10 ? ('0' + $minutes) : $minutes);
        $hours = floor(($i+$slice_to_minutes)/60);
        $minutes = ($i+$slice_to_minutes) - ($hours * 60);
        $end = ($hours < 10 ? ('0' + $hours) : $hours) . ':' . ($minutes < 10 ? ('0' + $minutes) : $minutes);
        $base_time = BaseTime::create([
          'start' => $start,
          'end' => $end,
          'available' => 1,
          'weekday' => $weekday->name
        ]);
        $base_time->save();
        $base_schedule->scheduleBaseTime()->save($base_time);
      }
    }

    flash('Itinerário "'. $base_schedule->name .'" criado com sucesso!', 'success');
    return redirect('/config/schedules/base_schedule');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $week_days = Weekday::get()->pluck('name')->all();
    $base_table = BaseSchedule::where('id', $id)->get()->first();

    foreach ($week_days as $key => $weekday) {
      if ($weekday == "sunday") $week_days[$key] = "domingo";
      if ($weekday == "monday") $week_days[$key] = "segunda";
      if ($weekday == "tuesday") $week_days[$key] = "terça";
      if ($weekday == "wednesday") $week_days[$key] = "quarta";
      if ($weekday == "thursday") $week_days[$key] = "quinta";
      if ($weekday == "friday") $week_days[$key] = "sexta";
      if ($weekday == "saturday") $week_days[$key] = "sábado";
    }

    if ($base_table) {
      $has_weekday = DB::select('select * from weekdays_schedule where baseschedule_id = :bid', ['bid' => $base_table->id]);
      $has_weekday = collect($has_weekday)
        ->pluck('weekday_id')
        ->toArray();
    }
    if ($base_table) {
      $base_week_days = [];
      foreach ($has_weekday as $weekday) {
        $day = Weekday::where('id', $weekday)->get()->first();
        $base_week_days[$day->name] = $day->name;
      }
      $slices = [];
      foreach ($base_week_days as $weekday) {
        array_push($slices, BaseTime::where('baseschedule_id', $base_table->id)->where('weekday', $weekday)->get()->all());
      }
      $plugin = new PluginControlTable();
    }

    foreach ($base_week_days as $key => $weekday) {
      if ($weekday == "sunday") $base_week_days[$key] = "domingo";
      if ($weekday == "monday") $base_week_days[$key] = "segunda";
      if ($weekday == "tuesday") $base_week_days[$key] = "terça";
      if ($weekday == "wednesday") $base_week_days[$key] = "quarta";
      if ($weekday == "thursday") $base_week_days[$key] = "quinta";
      if ($weekday == "friday") $base_week_days[$key] = "sexta";
      if ($weekday == "saturday") $base_week_days[$key] = "sábado";
    }

    return view('config_panel.schedule.show')->with([
     'week_days' => $week_days,
     'base_week_days' => (isset($base_week_days) ? $base_week_days : null),
     'base_table' => $base_table,
     'slices' => (isset($slices) ? $slices : null),
     'plugin' =>  (isset($plugin) ? $plugin : null),
     'return' => session()->has('return') ? session('return') : null
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $base_schedule = BaseSchedule::find($id);
    $name = $base_schedule->name;
    $base_schedule->delete();
    return redirect('/config/schedules/base_schedule')->with('status', 'Tabela ' . $name . ' deletada com sucesso!');
  }

  public function setActiveSchedulePage()
  {
    $tables = BaseSchedule::get();

    return view('config_panel.schedule.setActiveSchedule')->with(['tables' => $tables]);
  }

  public function loadScheduleModal(Request $request)
  {
    // return response()->json(['id' => $request['id']]);
    $week_days = Weekday::get()->pluck('name')->all();
    $table = BaseSchedule::find($request['id']);

    if ($table) {
      $has_weekday = DB::select('select * from weekdays_schedule where baseschedule_id = :bid', ['bid' => $table->id]);
      $has_weekday = collect($has_weekday)
        ->pluck('weekday_id')
        ->toArray();
    }
    if ($table) {
      $base_week_days = [];
      foreach ($has_weekday as $weekday) {
        $day = Weekday::where('id', $weekday)->get()->first();
        $base_week_days[$day->name] = $day->name;
      }
      $slices = [];
      foreach ($base_week_days as $weekday) {
        array_push($slices, BaseTime::where('baseschedule_id', $table->id)->where('weekday', $weekday)->get()->all());
      }
    }

    $response = view('config_panel.schedule.setActiveSchedule--modal')
      ->with([
        'plugin' => new \App\PluginControlTable,
        'table' => $table,
        'slices' => (isset($slices) ? $slices : null)
      ])->render();

    return response()->json(['title' => $table->name, 'body' => $response]);
  }

  public function setActiveSchedule($id, Request $request)
  {
    $activeTable = BaseSchedule::where('is_active', true)->first();
    
    if($activeTable)
    {
      $activeTable->is_active = false;
      $activeTable->save();
    }

    $selectedTable = BaseSchedule::find($id);

    if($selectedTable)
    {
      $selectedTable->is_active = true;
      $selectedTable->save();
    }

    flash("Itinerário atualizado para \"". $selectedTable->name ."\" com sucesso!", "success");
    return redirect()->route('config.schedule.setActiveSchedulePage');
  }
}
