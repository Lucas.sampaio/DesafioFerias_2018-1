<?php

namespace App\Http\Controllers\ConfigPanel\Schedule;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BaseSchedule;
use App\BaseTime;
use App\Weekday;
use DB;
use Validator;

class BaseTimeController extends Controller
{
    public function disable(Request $request, $id)
    {
      $base_time_slice = BaseSchedule::where('id', $id)->get()->pluck('slice');
      $base_time_slice = $base_time_slice[0];

      $rules = [
        'start_d' => 'required',
        'end_d' => 'required',
        'base_week_days_d' => 'required',
      ];
      $messages = [
          'start_d.required' => 'O campo "Início" não pode estar vazio!',
          'end_d.required' => 'O campo "Fim" não pode estar vazio!',
          'base_week_days_d.required' => 'O campo "Dias da semana" não pode estar vazio!',
      ];

      $validation = Validator::make($request->all(), $rules, $messages);

      for($i = 0; $i < count($request->base_week_days_d); $i++) {
        $weekday = Weekday::where('name', $request->base_week_days_d[$i])->get()->first();
        $weekday = DB::select('select * from weekdays_schedule where weekday_id = :weekday_id', ['weekday_id' => $weekday->id]);
        if (!$weekday)
          $validation->errors()->add('start_d', '"Dia da semana" não existe na tabela base');
      }

      $time = explode(":", $request->start_d);

      if (count($time) == 2) {
        $minutes_init = intval($time[0])*60 + intval($time[1]);
        $time = explode(":", $request->end_d);
        $minutes_end = intval($time[0])*60 + intval($time[1]);

        $slice_to_minutes = explode(":", $base_time_slice);
        $slice_to_minutes = intval($slice_to_minutes[0])*60 + intval($slice_to_minutes[1]);

        if ($minutes_end <= $minutes_init)
          $validation->errors()->add('end_d', '"Fim" deve ser menor que "Início"');

        $init = BaseTime::where('baseschedule_id', $id)->where('start', $request->start_d)->get()->first();
        if (!$init)
          $validation->errors()->add('start_d', '"Inicio" não existe na tabela base!');

        $final = BaseTime::where('baseschedule_id', $id)->where('end', $request->end_d)->get()->first();
        if (!$final)
          $validation->errors()->add('end_d', '"Fim" não existe na tabela base!');
      }

      if ($validation->errors()->messages()) {
        return redirect('/config/schedules/base_schedule/' . $id)->with([
            'errors' => $validation->errors(),
            'return' => $request->all()
        ]);
      }

      $minutes_init+=$slice_to_minutes;
      for($i = 0; $i < count($request->base_week_days_d); $i++) {
        $slice = BaseTime::where('baseschedule_id', $id)->where('start', $request->start_d)->where('weekday', $request->base_week_days_d[$i])->get()->first();
        $slice->available = 0;
        $slice->save();
        for ($j = $minutes_init; $j < $minutes_end; $j+=$slice_to_minutes) {
          $slice = BaseTime::where('id', $slice->id + 1)->get()->first();
          $slice->available = 0;
          $slice->save();
        }
      }

      return redirect('/config/schedules/base_schedule/' . $id)->with('status', 'Horários editados com sucesso!');
    }

    public function enable(Request $request, $id)
    {
      $base_time_slice = BaseSchedule::where('id', $id)->get()->pluck('slice');
      $base_time_slice = $base_time_slice[0];

      $rules = [
        'start_e' => 'required',
        'end_e' => 'required',
        'base_week_days_e' => 'required',
      ];
      $messages = [
          'start_e.required' => 'O campo "Início" não pode estar vazio!',
          'end_e.required' => 'O campo "Fim" não pode estar vazio!',
          'base_week_days_e.required' => 'O campo "Dias da semana" não pode estar vazio!',
      ];

      $validation = Validator::make($request->all(), $rules, $messages);

      for($i = 0; $i < count($request->base_week_days_e); $i++) {
        $weekday = Weekday::where('name', $request->base_week_days_e[$i])->get()->first();
        $weekday = DB::select('select * from weekdays_schedule where weekday_id = :weekday_id', ['weekday_id' => $weekday->id]);
        if (!$weekday)
          $validation->errors()->add('start_e', '"Dia da semana" não existe na tabela base');
      }

      $time = explode(":", $request->start_e);

      if (count($time) == 2) {
        $minutes_init = intval($time[0])*60 + intval($time[1]);
        $time = explode(":", $request->end_e);
        $minutes_end = intval($time[0])*60 + intval($time[1]);

        $slice_to_minutes = explode(":", $base_time_slice);
        $slice_to_minutes = intval($slice_to_minutes[0])*60 + intval($slice_to_minutes[1]);

        if ($minutes_end <= $minutes_init)
          $validation->errors()->add('end_e', '"Fim" deve ser menor que "Início"');

        $init = BaseTime::where('baseschedule_id', $id)->where('start', $request->start_e)->get()->first();
        if (!$init)
          $validation->errors()->add('start_e', '"Inicio" não existe na tabela base!');

        $final = BaseTime::where('baseschedule_id', $id)->where('end', $request->end_e)->get()->first();
        if (!$final)
          $validation->errors()->add('end_e', '"Fim" não existe na tabela base!');
      }

      if ($validation->errors()->messages()) {
        return redirect('/config/schedules/base_schedule/' . $id)->with([
            'errors' => $validation->errors(),
            'return' => $request->all()
        ]);
      }

      $minutes_init+=$slice_to_minutes;
      for($i = 0; $i < count($request->base_week_days_e); $i++) {
        $slice = BaseTime::where('baseschedule_id', $id)->where('start', $request->start_e)->where('weekday', $request->base_week_days_e[$i])->get()->first();
        $slice->available = 1;
        $slice->save();
        for ($j = $minutes_init; $j < $minutes_end; $j+=$slice_to_minutes) {
          $slice = BaseTime::where('id', $slice->id + 1)->get()->first();
          $slice->available = 1;
          $slice->save();
        }
      }

      return redirect('/config/schedules/base_schedule/' . $id)->with('status', 'Horários editados com sucesso!');
    }
}
