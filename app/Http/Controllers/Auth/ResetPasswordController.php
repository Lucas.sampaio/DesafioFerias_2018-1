<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;
use Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    use ResetsPasswords;
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('web');
     }

    // Formulário para editar senha
    public function index()
    {
      $user = Auth::user();
      return view('auth.passwords.reset');
    }

    // Função que valida a nova senha recebida e atualiza a senha do usuário
    public function reset(Request $request)
    {
      $user = Auth::user();

      $rules = [
        'password_new' => 'required|min:6|confirmed',
        'password_new_confirmation' => 'required|min:6'
      ];
      $messages = [
        'password_new.required' => 'O campo "Senha nova" não pode estar vazio!',
        'password_new.min' => 'O campo "Senha nova" não pode possuir menos que 6 caracteres!',
        'password_new.confirmed' => 'O campo "Confirmação" não condiz com senha nova!',
        'password_new_confirmation.required' => 'O campo "Confirmação" não pode estar vazio!',
        'password_new_confirmation.min' => 'O campo "Senha nova" não pode possuir menos que 6 caracteres!',
      ];

      $validation = Validator::make($request->all(), $rules, $messages);

      if (Hash::check($request->password, $user->password)) {
        if ($validation->fails()) {
          return redirect('password/reset/'. $user->id)->with([
            'errors' => $validation->errors()
          ]);
        }
        $user->update([
            'password' => $request->password_new,
        ]);
      } else {
        $validation->getMessageBag()->add('password', '"Senha atual" não condiz com a registrada');
        return redirect('password/reset/' . $user->id)->with([
          'errors' => $validation->errors()
        ]);
      }
      return redirect('auth/profile');
    }

    // Formulário para editar senha de link por email
    public function showResetByLink($token)
    {
      $token_mail = DB::select('select * from password_resets');

      $user_mail = null;
      foreach ($token_mail as $query) {
        if (Hash::check($token, $query->token)) {
          $user_mail = $query->email;
          break;
        }
      }

      if ($user_mail) {
        $user = User::where('email', $user_mail)->first();
        if (!$user) return redirect('/login')->with('status', 'Usuário inexistente!');
        return view('auth.passwords.email_reset')->with([
          'user' => $user,
          'token' => $token,
        ]);
      }

      return redirect('/login')->with('status', 'Link para redefinir senha experiou!');
    }

    // Função que valida a nova senha recebida do formulário de senha esquecida
    // e atualiza a senha do usuário com o respectivo token
    public function resetByLink($token, Request $request)
    {
      $token_mail = DB::select('select * from password_resets');

      $user_mail = null;
      foreach ($token_mail as $query) {
        if (Hash::check($token, $query->token)) {
          $user_mail = $query->email;
          break;
        }
      }

      $user = User::where('email', $user_mail)->first();

      $rules = [
        'password_new' => 'required|min:6|confirmed',
        'password_new_confirmation' => 'required|min:6'
      ];
      $messages = [
        'password_new.required' => 'O campo "Senha nova" não pode estar vazio!',
        'password_new.min' => 'O campo "Senha nova" não pode possuir menos que 6 caracteres!',
        'password_new.confirmed' => 'O campo "Confirmação" não condiz com "Senha nova"!',
        'password_new_confirmation.required' => 'O campo "Confirmação" não pode estar vazio!',
        'password_new_confirmation.min' => 'O campo "Senha nova" não pode possuir menos que 6 caracteres!',
      ];

      $validation = Validator::make($request->all(), $rules, $messages);

      if ($validation->fails()) {
        return redirect('password/email/reset/' . $request->_token)->with([
          'token' => $request->_token,
          'errors' => $validation->errors()
        ]);
      }

      $user->password = $request->password_new;
      $user->save();

      return redirect('/login')->with('status', 'Senha atualizada com sucesso!');
    }

    public function showFirstLoginReset($token)
    {
      $user = User::where('remember_token', $token)->first();
      return view('auth.passwords.first_reset')->with('user', $user);
    }

    public function resetFirstLogin($token, Request $request)
    {
      $user = User::where('remember_token', $token)->first();

      $rules = [
        'password_new' => 'required|min:6|confirmed',
        'password_new_confirmation' => 'required|min:6'
      ];
      
      $messages = [
        'password_new.required' => 'O campo "Senha nova" não pode estar vazio!',
        'password_new.min' => 'O campo "Senha nova" não pode possuir menos que 6 caracteres!',
        'password_new.confirmed' => 'O campo "Confirmação" não condiz com "Senha nova"!',
        'password_new_confirmation.required' => 'O campo "Confirmação" não pode estar vazio!',
        'password_new_confirmation.min' => 'O campo "Senha nova" não pode possuir menos que 6 caracteres!',
      ];

      $validation = Validator::make($request->all(), $rules, $messages);

      if ($validation->fails()) {
        return redirect('password/first/reset/' . $token)->with([
          'token' => $token,
          'errors' => $validation->errors()
        ]);
      }

      $user->password = $request->password_new;
      $user->save();

      return redirect('/auth/profile');
    }
}
