<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event;

class IndexController extends Controller
{
    public function index()
    {
        return view('index.index', [
            'inspire' => \Illuminate\Foundation\Inspiring::quote(),
        ]);
    }
}
