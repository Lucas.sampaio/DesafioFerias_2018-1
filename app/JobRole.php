<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobRole extends Model
{
    protected $fillable = ["name"];
}
